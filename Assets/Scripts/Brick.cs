using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {
	//public Sprite[] hitSprites;
	public Color[] hitColours;
	public AudioClip crack;
	public static int breakableCount = 0;
	public GameObject smoke;
	
	LevelManager levelManager;
	int timesHit;
	int maxHits;
	bool isBreakable;
	Color emptyColour = new Color(0f,0f,0f);
	
	// Use this for initialization
	void Start () {
		isBreakable = (this.tag == "Breakable");
		if(isBreakable){
			breakableCount++;
		}
		//maxHits = hitSprites.Length + 1;
		maxHits = hitColours.Length + 1;
		//Debug.Log ("hits: " + maxHits);
		timesHit = 0;	
		levelManager = GameObject.FindObjectOfType<LevelManager>();
	}
	
	void OnCollisionEnter2D(Collision2D collision){
		if(isBreakable){
			HandleHits();
		}
	}
	
	void HandleHits(){
		timesHit++;
		
		if (timesHit >= maxHits){
			breakableCount--;
			//Debug.Log("Count: " + breakableCount);
			AudioSource.PlayClipAtPoint(crack, transform.position, 1f);
			Instantiate(smoke, gameObject.transform.position, Quaternion.identity);
			levelManager.BrickDestoyed();
			Destroy(gameObject);
		}//else if(hitSprites[timesHit-1]){
		//	this.GetComponent<SpriteRenderer>().sprite = hitSprites[timesHit-1];
			//this.GetComponent<SpriteRenderer>().color = hitColours[timesHit-1];
		//}
		else if (hitColours.Length > 0 && hitColours[timesHit-1] != emptyColour){
			this.GetComponent<SpriteRenderer>().color = hitColours[timesHit-1];
		}
	}
	
	//TODO Remove 
	void SimulateWin(){
		levelManager.LoadNextLevel();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
}
