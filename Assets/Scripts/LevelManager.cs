﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public void QuitRequest () {
		//Debug.Log("Player selected quit");
		Application.Quit();
	}
	
	public void LoadLevel (string name) {
		//Debug.Log("Level load request for: " + name);
		Brick.breakableCount = 0;
		Application.LoadLevel(name);
	}
	
	public void LoadNextLevel(){
		Brick.breakableCount = 0;
		Application.LoadLevel(Application.loadedLevel + 1);
	}
	
	public void BrickDestoyed(){
		if(Brick.breakableCount <= 0){
			LoadNextLevel();
		}
	}
}
