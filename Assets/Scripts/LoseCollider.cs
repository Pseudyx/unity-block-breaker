﻿using UnityEngine;
using System.Collections;

public class LoseCollider : MonoBehaviour {
	LevelManager levelManager;
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();
	}
	
	void OnTriggerEnter2D(Collider2D collider){
		levelManager.LoadLevel("Lose");	
	}
}
