﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {
	public AudioClip[] playList;
	static MusicManager instance = null;
	int currentClipIndex;
	
	AudioSource PlayListManager{
		get {
			return instance.GetComponent<AudioSource>().GetComponent<AudioSource>();
		}
	}
	
	void Awake(){
		if(instance != null){
			Destroy(gameObject);
			//Debug.Log("duplicate music manager self-destroy");
		} else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			currentClipIndex = 0;
		}
	}
	
	// Use this for initialization
	void Start () {
				
	}
	
	// Update is called once per frame
	void Update () {
		bool isFinished = (!this.GetComponent<AudioSource>().isPlaying);
		
		if(isFinished){
			PlayNext();
		}
	}
	
	void PlayNext(){
		PlayListManager.Stop();
		currentClipIndex++;
		if(currentClipIndex >= playList.Length){
			currentClipIndex = 0;
		}
		PlayListManager.clip = playList[currentClipIndex];
		PlayListManager.Play();
	}
}
