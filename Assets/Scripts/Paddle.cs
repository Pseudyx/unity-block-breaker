﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {
	public float xMin, xMax;
	public bool autoPlay = false;
	Ball ball;
	
	void Start(){
		ball = GameObject.FindObjectOfType<Ball>();
	}
	
	// Update is called once per frame
	void Update () {
		if(autoPlay){
			AutoPlay();
		}
		else{
			MoveWithMouse();
		}
	}
	
	void MoveWithMouse(){
		float mousePos = Input.mousePosition.x/ Screen.width * 16;
		Move(mousePos);
	}
	
	void AutoPlay(){
		Vector3 ballPos = ball.transform.position;
		Move(ballPos.x);
	}
	
	void Move(float x){
		Vector3 paddlePos = new Vector3(0.5f, this.transform.position.y, 0f);
		paddlePos.x = Mathf.Clamp(x,xMin,xMax);
		this.transform.position = paddlePos;
	}
}
